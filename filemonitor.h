#ifndef FILEMONITOR_H
#define FILEMONITOR_H

#include <QObject>
#include <QString> 
#include <QMap>

class QFileSystemWatcher; 

class FileMonitor{

public:
      FileMonitor();
      ~FileMonitor();
      /**
      * @brief actionPath(s):  add and remove the path(s) of file from the monitoring system
      * @param filename(s): the file path(s) of the file 
      */
      int addPath(QString filename);
      int addPaths(QStringList filenames);
      int removePath(QString filename);
      int removePaths(QStringList filenames);
signals:
      /*
        @brief fileAction: the signal will be omitted when the file is created / deleted / modified.   
       */
      void fileCreated(const  QString filename);
      void fileDeleted(const  QString filename);
      void fileModified(const QString filename);
public slots:
       
protected:
      

private:   
      // the file system watcher.. 
      QFileSystemWatcher *pWatcher; 
      QMap<QString, int> filestatusMap; 
      int isFileExist(QString filename);
private slots:
      void slotWatcherFileChanged(const QString & path);
 
}

#endif 