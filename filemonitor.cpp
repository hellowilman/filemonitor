#include "filemonitor.h"
#include <QFileSystemWatcher>


FileMonitor::FileMonitor(){
    pWatcher = new QFileSystemWatcher();
    connect(pWatcher, SIGNAL(), this, SLOT(slotWatcherFileChanged(const QString & path)));
}

FileMonitor::~FileMonitor(){
    if(pWatcher){delete pWatcher;}
}

int FileMonitor::addPath(QString filename){
    int status = isFileExist(filename);
    if(status < 0){
        printf("error! cannot check the status of the files. Permission may be dennied\n");
        return -1; 
    }
    filestatusMap.insert(filename) = status;
    pWatcher->addPath(filename);
    return 0; 
}

int FileMonitor::addPaths(QStringList filenames){
    int rc = 0; 
    for(int it = filenames.size(); it >=0 ; it--){
        if(addPath(filenames[it])!= 0){
            rc = -1 - it; 
        };
    }
    return rc; 
}

int FileMonitor::removePath(QString filename){
    int rc = filestatusMap.remove(filename);
    pWatcher->removePath(filename);
    return 0; 
}

int FileMonitor::removePaths(QStringList filenames){

    int rc = 0; 
    for(int it = filenames.size(); it >=0 ; it--){
        if(removePath(filenames[it])!= 0){
            rc = -1 - it; 
        };
    }
    return rc; 
}

void FileMonitor::slotWatcherFileChanged(const QString & filename){
    if(filestatusMap.contains(filename)){
    	const int status = isFileExist(filename); 
    	if(status < 0){
        	printf("error! cannot check the status of the files. Permission may be dennied\n");
        	return -1; 
    	}
    	int sigFlag = status == filestatusMap.value(filename);
    	if(sigFlag && status > 0){
    	    //file exists 
    	    emit fileModified(filename); 
    	    return ; 
    	}
        if(!sigFlag && status == 0){
            //file deleted 
            emit fileDeleted(filename); 
            return ;
        }
        if(!sigFlag && status > 0 ){
            //file created 
            emit fileCreated(filename); 
            return ; 
        }
    }
}





